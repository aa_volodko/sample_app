# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'f81c129142cbbb898ab51bb91a3387f71333c0fd4799a7901892637b34a3dba7ea7ca29ae0421e8d45411ac028325cfc3ec3592d942091dc6bebdae99df61693'
